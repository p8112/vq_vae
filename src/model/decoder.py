import torch.nn as nn
from torch import Tensor
from .residual import ResidualStack


class Decoder(nn.Module):
    """
    Decoder block
    """
    def __init__(
            self, in_channels: int, hidden_channels: int,
            out_channels: int, num_layers: int, intermediate_channels: int
    ):
        """
        Init method
        :param in_channels: in channels
        :param hidden_channels: hidden channels
        :param out_channels: out channels
        :param num_layers: number of residual layers
        :param intermediate_channels: intermediate channels in residual
        """
        super(Decoder, self).__init__()

        self.decoder = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels, out_channels=hidden_channels,
                kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)
            ),
            nn.ReLU(inplace=True),
            ResidualStack(
                in_channels=hidden_channels, num_layers=num_layers,
                intermediate_channels=intermediate_channels
            ),
            nn.ConvTranspose2d(
                in_channels=hidden_channels,
                out_channels=hidden_channels // 2,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)
            ),
            nn.ReLU(inplace=True),
            nn.ConvTranspose2d(
                in_channels=hidden_channels // 2,
                out_channels=out_channels,
                kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)
            )
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Forward
        :param x: batch size, in channels, height, width
        :return: batch size, out channels, height * 4, width * 4
        """
        return self.decoder(x)
