from .encoder import Encoder
from .decoder import Decoder
from .vector_quantizer import VectorQuantizer
import torch.nn as nn
from torch import Tensor
from torch.nn.functional import mse_loss


class VQVAE(nn.Module):
    """
    VQVAE
    """
    def __init__(
            self, in_channels: int, hidden_channels: int,
            num_embeddings: int, embedding_dim: int,
            num_layers: int, intermediate_channels: int,
            commitment_factor: float, reconstruct_factor: float
    ):
        """
        Init method
        :param in_channels: number of in channels
        :param hidden_channels: number of hidden channels
        :param num_embeddings: number of embeddings
        :param embedding_dim: embedding layer
        :param num_layers: number of residual layers
        :param intermediate_channels: intermediate channels of residual
        :param commitment_factor: commitment factor
        :param reconstruct_factor: reconstruct factor
        """
        super(VQVAE, self).__init__()

        self.reconstruct_factor: float = reconstruct_factor

        self.encoder = Encoder(
            in_channels=in_channels, hidden_channels=hidden_channels,
            out_channels=embedding_dim,
            num_layers=num_layers, intermediate_channels=intermediate_channels
        )

        self.vq = VectorQuantizer(
            num_embeddings=num_embeddings, embedding_dim=embedding_dim,
            commitment_factor=commitment_factor
        )

        self.decoder = Decoder(
            in_channels=embedding_dim, hidden_channels=hidden_channels,
            out_channels=in_channels, num_layers=num_layers,
            intermediate_channels=intermediate_channels
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Predict reconstruct image
        :param x: batch size, num channels, height, width
        :return: batch size, num channels, height, width
        """
        z = self.encoder(x)
        z_q = self.vq(x=z)
        x_rec = self.decoder(z_q)
        return x_rec

    def training_step(self, x: Tensor) -> Tensor:
        """
        Training step
        :param x: batch size, num channels, height, width
        :return: scalar
        """
        z = self.encoder(x)
        z_q, quantized_loss = self.vq.training_step(x=z)
        x_rec = self.decoder(z_q)
        rec_loss = self.reconstruct_factor * mse_loss(input=x_rec, target=x)
        loss = quantized_loss + rec_loss
        return loss
