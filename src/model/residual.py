import torch.nn as nn
from torch import Tensor


class Residual(nn.Module):
    """
    Residual connection layer
    """
    def __init__(
            self, in_channels: int,
            intermediate_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param intermediate_channels: number channels of intermediate CONV layer
        """
        super(Residual, self).__init__()

        self.block = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=intermediate_channels,
                kernel_size=(3, 3), stride=(1, 1),
                padding=(1, 1), bias=False
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=intermediate_channels,
                out_channels=in_channels,
                kernel_size=(1, 1), stride=(1, 1), bias=False
            ),
            nn.ReLU(inplace=True)
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Forward
        :param x: batch size, num channels, height, width
        :return: batch size, num channels, height, width
        """
        return x + self.block(x)


class ResidualStack(nn.Module):
    """
    Stack multiple residual layers
    """
    def __init__(
            self, in_channels: int, intermediate_channels: int,
            num_layers: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param intermediate_channels: number channels of intermediate layers
        :param num_layers: number of layers
        """
        super(ResidualStack, self).__init__()
        self.layers = nn.ModuleList(
            [
                Residual(
                    in_channels=in_channels, intermediate_channels=intermediate_channels
                )
                for _ in range(num_layers)
            ]
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Forward
        :param x: batch size, num channels, height, width
        :return: batch size, num channels, height, width
        """
        for layer in self.layers:
            x = layer(x)
        return x


