import torch.nn as nn
from torch import Tensor
from typing import Tuple
import torch
from torch.nn.functional import mse_loss


class VectorQuantizer(nn.Module):
    """
    Vector quantizer layer
    """
    def __init__(
            self, num_embeddings: int, embedding_dim: int,
            commitment_factor: float
    ):
        """
        Init method
        :param num_embeddings: number of embeddings
        :param embedding_dim: embedding dim
        :param commitment_factor: factor for commitment loss
        """
        super(VectorQuantizer, self).__init__()

        self.num_embeddings: int = num_embeddings
        self.embedding_dim: int = embedding_dim
        self.commitment_factor: float = commitment_factor

        self.embedding = nn.Embedding(
            num_embeddings=self.num_embeddings,
            embedding_dim=self.embedding_dim
        )
        self.embedding.weight.data.uniform_(
            -1.0 / self.num_embeddings, 1.0 / self.num_embeddings
        )

    def find_nearest_neighbor(self, x: Tensor) -> Tensor:
        """
        Find nearest neighbor
        :param x: batch size, dim
        :return: batch size, num embeddings (one hot)
        """
        batch_size: int = x.shape[0]

        distances = torch.pow(
            x.unsqueeze(dim=1) - self.embedding.weight.unsqueeze(dim=0), 2
        ).sum(dim=2, keepdim=False)  # batch size, num embeddings

        indices = torch.argmin(
            input=distances, dim=1, keepdim=True
        )  # batch size, 1
        one_hot = torch.zeros(
            size=(batch_size, self.num_embeddings),
            device=x.device
        )  # batch size, num embeddings
        one_hot.scatter_(1, indices, 1)

        return one_hot

    def get_quantized(self, x: Tensor) -> Tensor:
        """
        Get quantized
        :param x: batch size, dim
        :return: batch size, dim
        """
        one_hot: Tensor = self.find_nearest_neighbor(
            x=x
        )  # batch size, num embeddings

        quantized = torch.matmul(
            one_hot, self.embedding.weight
        )   # batch size, embedding dim
        return quantized

    def forward(
            self, x: Tensor
    ) -> Tuple[Tensor, Tensor, Tensor, Tensor]:
        """
        Forward
        :param x: batch_size, num_channels, height, width
        :return:
            - batch_size, num_channels, height, width
            - scalar
        """
        batch_size, num_channels, height, width = x.shape

        x = x.permute(
            dims=(0, 2, 3, 1)
        )    # batch size, height, width, num channels
        x_flat = x.reshape(
            batch_size * height * width, num_channels
        )    # batch size * height * width, num channels

        quantized = self.get_quantized(x=x_flat).reshape(
            batch_size, height, width, num_channels
        )    # batch size, height, width, num channels
        quantized = quantized.permute(
            dims=(0, 3, 1, 2)
        )   # batch size, num channels, height, width

        return quantized

    def training_step(self, x: Tensor) -> Tuple[Tensor, Tensor]:
        """
        Training step
        :param x: batch_size, num_channels, height, width
        :return:
            - batch_size, num_channels, height, width
            - scalar
        """
        batch_size, num_channels, height, width = x.shape

        x = x.permute(
            dims=(0, 2, 3, 1)
        )    # batch size, height, width, num channels
        x_flat = x.reshape(
            batch_size * height * width, num_channels
        )    # batch size * height * width, num channels

        quantized = self.get_quantized(
            x=x_flat
        ).reshape(
            batch_size, height, width, num_channels
        )   # batch size, height, width, num channels

        q_latent_loss = mse_loss(input=quantized, target=x.detach())
        e_latent_loss = mse_loss(input=quantized.detach(), target=x)
        loss = q_latent_loss + self.commitment_factor * e_latent_loss

        quantized = x + (quantized - x).detach()
        quantized = quantized.permute(
            dims=(0, 3, 1, 2)
        )   # batch size, num channels, height, width

        return quantized, loss
