from torchvision.datasets import CIFAR10
import yaml
from typing import Dict, List
from torchvision.transforms import (
    Compose, ToTensor, Normalize
)
from torch.utils.data import DataLoader
from model import VQVAE
import numpy as np
import torch
from torch.optim import Adam
from tqdm import tqdm
from pathlib import Path
from torch.utils.tensorboard import SummaryWriter
import shutil
import matplotlib.pyplot as plt
from torch import Tensor
from torchvision.utils import make_grid
import math
import os


os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


# create dataset
transforms = Compose(
    transforms=[
        ToTensor(),
        Normalize(mean=[0.5, 0.5, 0.5], std=[1.0, 1.0, 1.0])
    ]
)
train_dataset = CIFAR10(
    root=config["data_dir"], train=True, download=True,
    transform=transforms
)
validation_dataset = CIFAR10(
    root=config["data_dir"], train=False, download=True,
    transform=transforms
)
train_loader = DataLoader(
    dataset=train_dataset, batch_size=config["train_batch_size"],
    shuffle=True, pin_memory=True
)
validation_loader = DataLoader(
    dataset=validation_dataset, batch_size=config["validation_batch_size"],
    shuffle=False, pin_memory=True
)


# create model
device = torch.device(config["device"])
model = VQVAE(
    in_channels=config["in_channels"],
    hidden_channels=config["hidden_channels"],
    intermediate_channels=config["intermediate_channels"],
    num_layers=config["num_layers"],
    num_embeddings=config["num_embeddings"],
    embedding_dim=config["embedding_dim"],
    reconstruct_factor=config["reconstruct_factor"],
    commitment_factor=config["commitment_factor"],
).to(device)
optimizer = Adam(
    params=model.parameters(), lr=config["learning_rate"],
    amsgrad=False
)


# define training, validation scripts
global_backward: int = 0
global_step: int = 0

Path(f"{config['output_dir']}/log").mkdir(parents=True, exist_ok=True)
writer = SummaryWriter(log_dir=f"{config['output_dir']}/log")


def clean_old_checkpoints():
    """
    Cleaning old checkpoint
    :return:
    """
    checkpoints: List[str] = [
        directory for directory in os.listdir(config["output_dir"])
        if directory.startswith("checkpoint")
    ]
    checkpoints: List[str] = sorted(
        checkpoints, key=lambda x: int(x.split("_")[1])
    )

    while len(checkpoints) >= config["checkpoints_limit"]:
        shutil.rmtree(f"{config['output_dir']}/{checkpoints[0]}")
        checkpoints = checkpoints[1:]


def save_model(directory: str):
    """
    Execute save model
    :param directory: directory to save
    :return:
    """
    Path(directory).mkdir(parents=True, exist_ok=True)
    torch.save(
        model.state_dict(), f"{directory}/model.pt"
    )


def draw_image(
        image: Tensor, ax: plt.Axes, title: str
) -> plt.Axes:
    """
    Draw image
    :param image: image to draw
    :param ax: ax
    :param title: title of ax
    :return: ax
    """
    image: np.ndarray = image.permute(dims=(1, 2, 0)).numpy()
    ax.imshow(image, interpolation='nearest')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_title(title)
    return ax


@torch.no_grad()
def validate_epoch():
    """
    Generate image
    :return:
    """
    model.eval()

    for x, _ in validation_loader:
        fig: plt.Figure = plt.figure(figsize=(20, 20))

        x_rec = model(x.to(device)).cpu().detach()
        draw_image(
            image=make_grid(
                tensor=x_rec + 0.5,
                nrow=int(math.sqrt(config["validation_batch_size"]))
            ),
            ax=fig.add_subplot(1, 1, 1),
            title="Reconstruct images"
        )
        writer.add_figure(
            tag="reconstruct validation images", figure=fig, global_step=global_step
        )
        plt.close()
        break


def training_epoch(epoch: int) -> float:
    """
    Execute a training epoch
    :param epoch: epoch
    :return: loss
    """
    global global_step, global_backward
    model.train()

    losses: List[float] = []

    progress_bar = tqdm(
        iterable=train_loader, desc=f"Training epoch {epoch}..."
    )
    for x, _ in progress_bar:
        loss = model.training_step(x=x.to(device))

        global_backward += 1
        loss.backward()

        if global_backward % config["accumulate_gradient_steps"] == 0:
            optimizer.step()
            optimizer.zero_grad()
            global_step += 1

        loss_value: float = loss.item()
        losses.append(loss_value)
        writer.add_scalar(
            tag="train_loss", scalar_value=loss_value, global_step=global_backward
        )
        if global_backward % config["update_bar_interval"] == 0:
            progress_bar.set_description(
                f"Training epoch {epoch}: loss {loss_value:.4f}..."
            )
            progress_bar.update()

        if global_step % config["checkpoint_interval"] == 0:
            clean_old_checkpoints()
            save_model(
                directory=f"{config['output_dir']}/checkpoint_{global_step}"
            )

        if global_step % config["validation_interval"] == 0:
            validate_epoch()

        if global_step >= config["max_steps"]:
            break
    progress_bar.close()

    return sum(losses) / len(losses)


# log validation image
for val_images, _ in validation_loader:
    figure: plt.Figure = plt.figure(figsize=(20, 20))

    draw_image(
        image=make_grid(
            tensor=val_images + 0.5,
            nrow=int(math.sqrt(config["validation_batch_size"]))
        ),
        ax=figure.add_subplot(1, 1, 1),
        title="Original images"
    )
    writer.add_figure(
        tag="origin validation images", figure=figure, global_step=0
    )
    plt.close()
    break


# train
for i in range(config["max_epochs"]):
    train_loss: float = training_epoch(epoch=i)
    print(f"Finish epoch {i}: loss = {train_loss:.4f}")

    if global_step >= config["max_steps"]:
        break


# save final model
save_model(
    directory=f"{config['output_dir']}/final_checkpoint"
)


# log test image
offset: int = 0
for val_images, _ in validation_loader:
    offset += 1
    if offset == 1:
        continue

    figure: plt.Figure = plt.figure(figsize=(40, 20))

    draw_image(
        image=make_grid(
            tensor=val_images + 0.5,
            nrow=int(math.sqrt(config["validation_batch_size"]))
        ),
        ax=figure.add_subplot(1, 2, 1),
        title="Original images"
    )

    model.eval()
    val_images_rec = model(val_images.to(device)).cpu().detach()
    draw_image(
        image=make_grid(
            tensor=val_images_rec + 0.5,
            nrow=int(math.sqrt(config["validation_batch_size"]))
        ),
        ax=figure.add_subplot(1, 2, 2),
        title="Reconstruct images"
    )

    writer.add_figure(
        tag="Test images", figure=figure, global_step=0
    )
    break

writer.flush()
writer.close()
